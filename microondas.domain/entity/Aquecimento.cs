﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace microondas.domain.entity
{
    public class Aquecimento
    {
        public Aquecimento(string tempo, string potencia)
        {
            this.Tempo = tempo;
            this.Potencia = potencia == null ? "10" : potencia;
        }
        public string Tempo { get; set; }
        public string Potencia { get; set; }
    }
}
