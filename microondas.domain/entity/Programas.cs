﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace microondas.domain.entity
{
    public class Programas
    {
        
        public string Nome { get; set; }
        public string Instrucao { get; set; }
        public Aquecimento Aquecimento { get; set; }

        public static IList<Programas> GerarProgramas()
        {
            IList<Programas> programas = new List<Programas>();

            programas.Add(new Programas
            {
                Nome = "frango, peixe",
                Instrucao = "Apenas carnes brancas",
                Aquecimento = new Aquecimento("15", "3")

            });

            programas.Add(new Programas
            {
                Nome = "Carnes, picanha, linguiça",
                Instrucao = "Apenas carnes vermelhas",
                Aquecimento = new Aquecimento("20", "5")

            });

            programas.Add(new Programas
            {
                Nome = "água, café, leite",
                Instrucao = "Apenas liquídos",
                Aquecimento = new Aquecimento("10", "1")

            });

            programas.Add(new Programas
            {
                Nome = "pipoca ",
                Instrucao = "pipocas de microondas doces e salgadas.",
                Aquecimento = new Aquecimento("30", "8")

            });

            programas.Add(new Programas
            {
                Nome = "Descongelamento",
                Instrucao = "descongelamento de carnes acima de 1KG.",
                Aquecimento = new Aquecimento("20", "3")

            });
            return programas;
        }
    }
}
