﻿using microondas.domain.entity;
using microondas.mvc.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace microondas.mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public JsonResult GetProgramas()
        {

            var programas = Programas.GerarProgramas();
            Session.Add("programas", programas);

            return Json(programas, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Pesquisar(string programa)
        {
            try
            {
                var programas = Session["programas"] as List<Programas>;
                var obj = programas.Where(x => x.Nome.ToUpper().Contains(programa.ToUpper())).FirstOrDefault();
                if (obj == null)
                    throw new Exception("alimento incompatível com o programa.");
                return Json(new { Sucesso = true, obj }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = false, erro = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AdicionarPrograma(string nome, string instrucao, string tempo, string potencia)
        {
            try
            {
                if (String.IsNullOrEmpty(nome))
                    throw new Exception("Todos campos são obrigatórios.");
                if (String.IsNullOrEmpty(instrucao))
                    throw new Exception("Todos campos são obrigatórios.");
                if (String.IsNullOrEmpty(tempo))
                    throw new Exception("Todos campos são obrigatórios.");
                if (String.IsNullOrEmpty(potencia))
                    throw new Exception("Todos campos são obrigatórios.");

                if(Convert.ToInt32(potencia) > 10)
                    throw new Exception("Potência máxima nao pode ser maior que 10.");

                if(Convert.ToInt32(tempo) > 120)
                    throw new Exception("Tempo máximo nao pode ser maior que 2 minutos.");

                var programas = Session["programas"] as List<Programas>;
                programas.Add(
                    new Programas
                    {
                        Nome = nome,
                        Instrucao = instrucao,
                        Aquecimento = new Aquecimento(tempo, potencia)
                    });

                return Json(new { Sucesso = true, programas }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = false, erro = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Cozinhar(MicroondaViewModel model)
        {
            try
            {
                if (model.Potencia == null)
                    model.Potencia = "10";
                if (String.IsNullOrEmpty(model.Tempo))
                    throw new Exception("Campo Tempo de cozimento é de preenchimento obrigatório!");
                if (Convert.ToInt32(model.Tempo) > 200)
                    throw new Exception("Tempo máximo de cozimento é de 2 minutos.");
                if (Convert.ToInt32(model.Potencia) > 10)
                    throw new Exception("Potência máxima é 10.");

                return Json("");

            }
            catch (Exception ex)
            {
                return Json(new { Sucesso = false, msg = ex.Message });
            }

        }
    }
}