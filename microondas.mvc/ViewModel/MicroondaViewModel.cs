﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace microondas.mvc.ViewModel
{
    public class MicroondaViewModel
    {
        [DisplayName("Tempo de cozimento")]
        public string Tempo { get; set; }

        [DisplayName("Potência")]
        public string Potencia {
            get;
            set;
        }


    }
}